#include "luascript.h"
#include <iostream>
#include <string>
#include <string.h>
#include "../game/level.h"
#include "../components/component.h"
#include "../allegro/graphics.h"

void LuaVM::init(const char* filename){
    m_pState = luaL_newstate();
    luaL_openlibs(m_pState);
    int result = luaL_loadfile(m_pState, filename);

	lua_pushcfunction(m_pState, lua_create_level);
    lua_setglobal(m_pState, "_createLevel");
  //lua_pushcfunction(state, lua_create_object);
  //lua_setglobal(state, "_createObject");
    lua_pushcfunction(m_pState, lua_create_object);
    lua_setglobal(m_pState, "_createObject");
    lua_pushcfunction(m_pState, lua_add_component);
    lua_setglobal(m_pState, "_addComponent");
    lua_pushcfunction(m_pState, lua_move_to);
    lua_setglobal(m_pState, "_moveTo");


    if(result != 0){
		printf("Erro script: %s\n ",lua_tostring(m_pState,-1));
    }
    lua_pcall(m_pState, 0, LUA_MULTRET, 0);
}

void LuaVM::close(){
	lua_close(m_pState);
}

void LuaVM::callFunction(const char* name){
	lua_getglobal(m_pState, name);
	lua_pcall(m_pState, 0, 1, 0);
	lua_pop(m_pState, 1);
}

lua_State* LuaVM::getState(){
	return m_pState;
}

//Lua c functions
//
int lua_create_level(lua_State *state){
    printf("_create_level\n");
    Level* level = LevelManager::create();
    lua_pushlightuserdata(state, level);
    return 1;
}

//int lua_create_object(lua_State *state){
//    printf("_create_object\n");
//    Level* level = (Level*)lua_touserdata(state, 1);
//    int x = lua_tointeger(state, 2);
//    int y = lua_tointeger(state, 3);
//    GameObject *object = level->newGameObject();
//    object->setPosition(Vector2(x,y));
//    lua_pushlightuserdata(state, object);
//    return 1;
//}

int lua_add_component(lua_State *state){
    printf("_add_component\n");
    GameObject* object = (GameObject*)lua_touserdata(state, 1);
    const char* type = lua_tostring(state, 2);
    if(strcmp(type,"staticSprite")==0){
        printf("  _add_static_sprite\n");
        const char* texture = lua_tostring(state, 3);
        auto ss = object->addComponent<StaticSprite>();
        ss->setSprite(Graphics::getInstance()->getTexture(texture),Rectangle(0,0,32,32));
    }
    return 0;
}

int lua_create_object(lua_State *state){
    printf("_create_object_ext\n");
    Level* level = (Level*)lua_touserdata(state, 1);
    float x = lua_tonumber(state, 2);
    float y = lua_tonumber(state, 3);
    int len = lua_rawlen(state, 4);

    GameObject *object = level->newGameObject();
    object->setPosition(Vector2(x,y));

    std::cout<<"len "<<len<<"\n\n";
    for(int i=1; i <= len;i++){
        printf("i = %d\n",i);
        lua_rawgeti(state, -1, i);
        lua_getfield(state, 5,"type");
        const char* type = lua_tostring(state,-1);
        //unsigned int type = lua_tointeger(state,-1);
        lua_pop(state, 1);

        if(strcmp(type,"playerBehaviour")==0){
            object->addComponent<PlayerController>();
        }
        else if(strcmp(type,"scriptBehaviour")==0){
            printf("scriptSetState\n");
            auto s = object->addComponent<ScriptBehaviour>();
            s->setState(state);

        }else if (strcmp(type,"boxCollider")==0){
			printf("add BoxCollider");
			auto bc = object->addComponent<BoxCollider>();
			bc->setSize(32,32);

		}else if (strcmp(type,"staticSprite")==0){

             lua_getfield(state,5,"x");
             int sx = lua_tointeger(state, -1);
             lua_pop(state, 1);

             lua_getfield(state,5,"y");
             int sy = lua_tointeger(state, -1);
             lua_pop(state, 1);

             lua_getfield(state,5,"w");
             int sw = lua_tointeger(state, -1);
             lua_pop(state, 1);

             lua_getfield(state,5,"h");
             int sh = lua_tointeger(state, -1);
             lua_pop(state, 1);

             lua_getfield(state,5,"texture");
             const char* texture = lua_tostring(state, -1);
             lua_pop(state, 1);

             auto ss = object->addComponent<StaticSprite>();
             ss->setSprite(Graphics::getInstance()->getTexture(texture),{sx,sy,sw,sh});

        }
        printf("TYPE: %s\n\n",type);
        lua_pop(state,1);
    }
     lua_pushlightuserdata(state, object);
    return 1;
}

int lua_move_to(lua_State *state){
    printf("_moveTo\n");
    ScriptBehaviour* s = (ScriptBehaviour*)lua_touserdata(state, 1);
    int x = lua_tointeger(state, 2);
    int y = lua_tointeger(state, 3);
    int sx = lua_tointeger(state, 4);
    int sy = lua_tointeger(state, 5);
    s->move_to(Vector2(x,y),Vector2(sx,sy));
    return 1;
}
